import objectAssign from 'object-assign';
import initialState from './initialState';
import * as types from '../actions/constants/actionTypes';

// Handles video related actions
// The idea is to return an updated copy of the state depending on the action type.
export default function (state = initialState.users, action) {
  let newState
  switch (action.type) {
    case types.GET_USERS_SUCCESS:
      newState = objectAssign({}, state, action.users);
      return newState;
    case types.GET_USERS_ERROR:
          newState = objectAssign({}, state, users = [], action.error)
    default:
      return state;
  }
}

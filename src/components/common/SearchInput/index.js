import React, {PropTypes} from 'react';

const SearchInput = (props)=>{
  return(
    <div className="container">
      <div className="row">
        <h2>GitHub Users Search Box</h2>
        <div className="custom-search-input">
          <div className="input-group col-md-12">
            <input type="text" className="search-query form-control" placeholder="Search" />
                                <span className="input-group-btn">
                                    <button className="btn btn-success" type="button">
                                      <span className=" glyphicon glyphicon-search"></span>
                                    </button>
                                </span>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SearchInput;

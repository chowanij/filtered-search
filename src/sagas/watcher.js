import { takeLatest } from 'redux-saga';
import { searchUsersSaga } from './usersSaga';
import * as types from '../actions/constants/actionTypes';

export default function* watchSearchUsers() {
  yield* takeLatest(types.GET_USERS_SUCCESS, searchUsersSaga);
}

import { put, call } from 'redux-saga/effects';
import { allGitHubUsers } from '../api/api';
import * as types from '../actions/constants/actionTypes';

export function* searchUsersSaga({query}){
  try {
    const users = yield call(allGitHubUsers, query);
    yield [
      put({ type: types.GET_USERS_SUCCESS, users })
    ];
  } catch (error) {
    yield put({ type: types.GET_USERS_ERROR, error });
  }
}


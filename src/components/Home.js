import React from "react";
import SearchInput from "./common/SearchInput";
import UsersSearchContainer from "../containers/UsersSearchContainer";

// Home page component
export default class Home extends React.Component {
  // render
  render() {
    return (
      <div className="page-home">
        <UsersSearchContainer/>
        <SearchInput/>
        <h4>Some content</h4>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ultricies laoreet lectus sit amet pulvinar. Ut sollicitudin risus at sapien accumsan maximus. Etiam nec mauris in orci pharetra tempor. Donec magna orci, egestas sed mauris non, scelerisque tincidunt mauris. Aliquam nec finibus risus, vel mattis justo. Ut tincidunt ex odio, eget ullamcorper magna tempor quis. Proin ligula est, ullamcorper non dui eget, fermentum consequat ex. Sed ultricies at elit eu tincidunt. Morbi neque lectus, sodales sed risus et, lobortis facilisis elit. Quisque sit amet efficitur elit. Nam eget sodales dolor, at cursus risus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. In augue nibh, lacinia vitae ullamcorper sed, pellentesque dictum nunc.
        </p>
        <p>
          Cras tempor mi ut quam porttitor, et tempor dolor lobortis. Praesent enim augue, fermentum vitae suscipit a, rutrum et sapien. Vivamus consequat, neque ut imperdiet semper, leo quam iaculis elit, eget pharetra eros nisl vel ligula. In viverra metus sed tempus bibendum. Etiam rutrum magna et mi tristique, et pharetra mi mattis. Aliquam felis tellus, tincidunt et magna ac, cursus suscipit felis. Cras molestie, risus sed ornare accumsan, elit metus ultricies dolor, vitae luctus augue purus in nibh. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.
        </p>
      </div>
    );
  }
}

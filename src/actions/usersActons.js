import * as types from './constants/actionTypes';

export const getAllUsersAction = (users) => ({
  type: types.GET_ALL_USERS_SUCCESS,
  users
});
